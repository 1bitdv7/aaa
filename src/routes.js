const { Router } = require("express");
const router = new Router();
const homeController = require("./app/controllers/homeController");
const dashboardController = require("./app/controllers/dashboardController");

router.get("/", homeController.index);
router.get("/find:slug", homeController.listAllByCategory);

router.get("/dashboard", dashboardController.index);
router.get("/dashboard/addCategory", dashboardController.showCategory);
router.post("/dashboard/save", dashboardController.save);

router.get("/dashboard/category/edit/:id", dashboardController.showEditCategory);
router.post("/dashboard/updateCategory",dashboardController.updateCategory)

router.post("/dashboard/category/delete/:id",dashboardController.deleteCategory)
module.exports = router;
