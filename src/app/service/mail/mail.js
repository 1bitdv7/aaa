const nodemailer = require('nodemailer')
const MailerConfig = require('../../config/config.mail')
module.exports = nodemailer.createTransport(MailerConfig)