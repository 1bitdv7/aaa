
class Dashboard {
  async index(req, res) {
    
    res.render("dashboard/showCategory", { categories });
  }
  showCategory(req, res) {
    res.render("dashboard/addCategory");
  }
  async save(req, res) {
    
  }

  async showEditCategory(req, res) {
   
  }
  async updateCategory(req, res) {
   
  }
  async deleteCategory(req, res) {
    
  }
}

module.exports = new Dashboard();
